using System;

using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game;

public class ThirdPersonController : MonoBehaviour
{
	public static ThirdPersonController Instance { get; private set; }
	
	[NonSerialized]
	public bool CanControl;

	[NonSerialized]
	public bool CanJump;

	[NonSerialized]
	public int ForwardSpeedMax = Karen90MmoFramework.GlobalGameSettings.PLAYER_FORWARD_SPEED_MAX;

	[NonSerialized]
	public int SideSpeedMax = Karen90MmoFramework.GlobalGameSettings.PLAYER_SIDE_SPEED_MAX;
	
	[NonSerialized]
	public int BackwardSpeedMax = Karen90MmoFramework.GlobalGameSettings.PLAYER_BACKWARD_SPEED_MAX;

	public AnimationClip Idle;
	public AnimationClip Run;
	public AnimationClip Walk;
	public AnimationClip StrafeLeft;
	public AnimationClip StrafeRight;

	public bool CanRotateCharacter { get; set; }

	private CharacterAnimationState animationState;
	private MovementKeys keys;
	private int speed;

	public int Speed
	{
		get { return this.speed; }
	}

	protected float yRotation
	{
		get { return this.transform.rotation.eulerAngles.y; }
		private set
		{
			var rot = this.transform.rotation;
			this.transform.rotation = Quaternion.Euler(rot.x, value, rot.z);
		}
	}

	public float Orientation { get; set; }

	public MovementKeys Keys
	{
		get { return this.keys; }
	}

	private CharacterController controller;
	void Awake()
	{
		Instance = this;

		this.CanControl = false;
		this.CanJump = false;

		this.controller = this.GetComponent<CharacterController>();
		this.GetComponent<Animation>().AddClip(GetComponent<Animation>()[Walk.name].clip, "walkBackwards");
		this.GetComponent<Animation>()["walkBackwards"].speed = -1;
	}

	void Start()
	{
		this.animationState = CharacterAnimationState.Idle;
	}

	void Update()
	{
		if (!CanControl)
			return;

		// calculates movement weights
		var inputX = (int)Input.GetAxisRaw("Horizontal");
		var inputZ = (int)Input.GetAxisRaw("Vertical");

		var lSpeed = 0;
		var lKeys = MovementKeys.None;

		if (inputX > 0)
		{
			lSpeed = this.SideSpeedMax;
			this.animationState = CharacterAnimationState.StrafingRight;

			lKeys |= MovementKeys.TurnRight;
		}
		else if (inputX < 0)
		{
			lSpeed = this.SideSpeedMax;
			this.animationState = CharacterAnimationState.StrafingLeft;

			lKeys |= MovementKeys.TurnLeft;
		}

		// handle forward and backward last to change side speed depending on forward backward speed
		if (inputZ > 0)
		{
			lSpeed = this.ForwardSpeedMax;
			this.animationState = CharacterAnimationState.Running;

			lKeys |= MovementKeys.Steer;
		}
		else if (inputZ < 0)
		{
			lSpeed = this.BackwardSpeedMax;
			this.animationState = CharacterAnimationState.Backing;

			lKeys |= MovementKeys.Reverse;
		}
		
		if(Input.GetKeyDown(KeyCode.Space))
			lKeys |= MovementKeys.Jump;

		if (Input.GetKey(KeyCode.LeftShift))
			lKeys |= MovementKeys.Shift;

		if (CanRotateCharacter)
		{
			// Making the character face the same direction as the camera
			var newRotation = this.Orientation;
			if (Math.Abs(yRotation - newRotation) > 0.1f)
			{
				this.yRotation = newRotation;
			}
		}
		
		// moves the character depending on the direction and the speed
		if ((lKeys & MovementKeys.MoveKeys) != MovementKeys.None)
		{
			// calculates the normal for the velocity and converts it into a direction normal. to keep the speed a constant
			var directionNormal = this.transform.TransformDirection(new Vector3(inputX, 0, inputZ).normalized);
			var velocity = directionNormal * lSpeed * Time.deltaTime;

			controller.Move(velocity);
		}
		else
		{
			this.animationState = CharacterAnimationState.Idle;
		}

		this.speed = lSpeed;
		this.keys = lKeys;

		this.HandleAnimation();
	}

	void HandleAnimation()
	{
		switch (this.animationState)
		{
			case CharacterAnimationState.Idle:
				{
					this.GetComponent<Animation>().CrossFade(this.Idle.name);
				}
				break;

			case CharacterAnimationState.Running:
				{
					this.GetComponent<Animation>().CrossFade(this.Run.name);
				}
				break;

			case CharacterAnimationState.Backing:
				{
					this.GetComponent<Animation>().CrossFade("walkBackwards");
				}
				break;
				
			case CharacterAnimationState.StrafingLeft:
				{
					this.GetComponent<Animation>().CrossFade(this.StrafeLeft.name);
				}
				break;

			case CharacterAnimationState.StrafingRight:
				{
					this.GetComponent<Animation>().CrossFade(this.StrafeRight.name);
				}
				break;
		}
	}
};