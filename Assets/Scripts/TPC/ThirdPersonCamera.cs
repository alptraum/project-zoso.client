using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
	public static ThirdPersonCamera Instance { get; private set; }

	public Transform MainCamera { get; set; }

	public Vector3 headOffset = Vector3.zero;

	public bool CanScroll { get; set; }
	public bool CanRotate { get; set; }
	
	public float Distance = 5.0f;
	public float DistanceMin = 4.0f;
	public float DistanceMax = 12.0f;
	public float DistanceSmoothDelta = 0.01f;

	public float YRotationMin = -40;
	public float YRotationMax = 80;

	public float ScrollSensitivity = 0.5f;
	public float MouseSensitivity = 5.0f;

	private Vector3 lookAtPosition;
	private float dampVelocity;

	private float mouseX;
	private float mouseY;

	private Vector3 targetLocation = Vector3.zero;

	void Awake()
	{
		Instance = this;

		CanScroll = false;
		CanRotate = false;
	}

	void Start()
	{
		this.assignAvailableCamera();
	}

	void LateUpdate()
	{
		this.lookAtPosition = this.transform.position + headOffset;

		this.handleScrollDistance();
		this.handleRotation();
		this.updatePositions();

		this.targetLocation = this.calculatePosition(this.mouseX, this.mouseY, this.Distance);
	}

	public void ResetRotation()
	{
		this.mouseX = ThirdPersonController.Instance.Orientation;
		this.mouseY = 15;
	}

	private void handleScrollDistance()
	{
		if (CanScroll)
		{
			var distanceDelta = Input.GetAxis("Mouse ScrollWheel");
			var distance = this.Distance;

			if (distanceDelta < -0.01f || distanceDelta > 0.01f)
			{
				distance = Mathf.Clamp(this.Distance - distanceDelta*this.ScrollSensitivity, this.DistanceMin, this.DistanceMax);
			}

			this.Distance = Mathf.SmoothDamp(this.Distance, distance, ref dampVelocity, DistanceSmoothDelta);
		}
	}

	private void handleRotation()
	{
		if (CanRotate)
		{
			if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
			{
				var dx = Input.GetAxis("Mouse X");
				var dy = Input.GetAxis("Mouse Y");

				if (Mathf.Abs(dx) > 0 || Mathf.Abs(dy) > 0)
				{
					ThirdPersonController.Instance.CanRotateCharacter = Input.GetMouseButton(1);

					this.mouseX += dx * this.MouseSensitivity;
					this.mouseY -= dy * this.MouseSensitivity;
				}
			}

			this.mouseY = Mathf.Clamp(this.mouseY, this.YRotationMin, this.YRotationMax);
		}
	}

	private void updatePositions()
	{
		this.MainCamera.position = lookAtPosition + this.targetLocation;
		this.MainCamera.LookAt(this.lookAtPosition);
	}

	private Vector3 calculatePosition(float x, float y, float distance)
	{
		var dir = new Vector3(0, 0, -distance);
		var rotation = Quaternion.Euler(y, x, 0);

		return rotation * dir;
	}

	private void assignAvailableCamera()
	{
		if (this.MainCamera == null)
			this.MainCamera = Camera.main.transform;

		if (this.MainCamera == null)
		{
			var cameraObject = new GameObject("MainCamera");
			cameraObject.AddComponent<GamePersister>();
			this.MainCamera = cameraObject.AddComponent<Camera>().transform;
		}
	}
};
