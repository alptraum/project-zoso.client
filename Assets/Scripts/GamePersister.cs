﻿using UnityEngine;

/// <summary>
/// An object which will persist through scene loads throughout the game and will be destroyed when the game handler changes
/// </summary>
public class GamePersister : MonoBehaviour
{
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public void Destroy()
	{
		Destroy(gameObject);
	}
}
