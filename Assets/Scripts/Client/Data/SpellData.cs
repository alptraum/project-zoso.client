﻿using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game;

namespace Karen90MmoFramework.Client.Data
{
	public class SpellData
	{
		public short SpellId { get; set; }
		public string Name { get; set; }

		public SpellSchools School { get; set; }
		public SpellTargetTypes RequiredTargetType { get; set; }
		public WeaponTypes RequiredWeaponType { get; set; }
		public SpellTargetSelectionMethods TargetSelectionMethod { get; set; }

		public bool AffectedByGCD { get; set; }
		public bool IsProc { get; set; }
		public bool TriggersGCD { get; set; }

		public Vitals PowerType { get; set; }
		public int PowerCost { get; set; }

		public float Cooldown { get; set; }
		public float CastTime { get; set; }

		public int MinCastRadius { get; set; }
		public int MaxCastRadius { get; set; }

		public string Description { get; set; }
		public short IconId { get; set; }

		Texture2D icon;
		GUIContent drawContent;

		/// <summary>
		/// Gets the <see cref="UnityEngine.GUIContent"/>.
		/// </summary>
		public GUIContent DrawContent
		{
			get
			{
				if (drawContent == null)
					this.drawContent = new GUIContent(this.Icon, TooltipStyle.Spell + "#" + this.SpellId);

				return this.drawContent;
			}
		}

		/// <summary>
		/// Gets the item icon.
		/// </summary>
		public Texture2D Icon
		{
			get
			{
				if (icon == null && IconId > 0)
					this.icon = GameResources.Instance.LoadIcon(IconId);

				return this.icon;
			}
		}
	}
}
