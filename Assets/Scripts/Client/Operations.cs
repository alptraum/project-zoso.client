﻿using System;
using System.Collections.Generic;

using Karen90MmoFramework.Rpc;
using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client
{
	public static class Operations
	{
		private static readonly NetworkEngine _engine = NetworkEngine.Instance;

		/// <summary>
		/// Sends a login user request
		/// </summary>
		public static void LoginUser(string username, string password)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LoginUser},
					{(byte) ParameterCode.Username, username},
					{(byte) ParameterCode.Password, password},
				};

			_engine.SendOperation(ClientOperationCode.Login, parameters, true);
		}

		/// <summary>
		/// Sends a logout user request
		/// </summary>
		public static void LogoutUser()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LogoutUser},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends a create user request
		/// </summary>
		public static void CreateUser(string username, string password)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.CreateUser},
					{(byte) ParameterCode.Username, username},
					{(byte) ParameterCode.Password, password},
				};

			_engine.SendOperation(ClientOperationCode.Login, parameters, true);
		}

		/// <summary>
		/// Sends a login character request
		/// </summary>
		public static void LoginCharacter(string characterName)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LoginCharacter},
					{(byte) ParameterCode.CharacterName, characterName},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends a logout character request
		/// </summary>
		public static void LogoutCharacter()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LogoutCharacter},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends a create character request
		/// </summary>
		public static void CreateCharacter(CharacterStructure characterData)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.CreateCharacter},
					{(byte) ParameterCode.Data, characterData},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends a delete character request
		/// </summary>
		public static void DeleteCharacter(string characterName)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.DeleteCharacter},
					{(byte) ParameterCode.CharacterName, characterName},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends a retrieve characters request
		/// </summary>
		public static void RetrieveCharacters()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.RetrieveCharacters},
				};

			_engine.SendOperation(ClientOperationCode.Character, parameters);
		}

		/// <summary>
		/// Sends an enter world request
		/// </summary>
		public static void EnterWorld()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.EnterWorld},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a move request
		/// </summary>
		public static void Move(MovementKeys keys)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.Move},
					{(byte) ParameterCode.Keys, (byte) keys},
					{(byte) ParameterCode.SentTime, _engine.ServerTimeStamp},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a rotate request
		/// </summary>
		public static void Rotate(float orientation)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.Rotate},
					{(byte) ParameterCode.Orientation, orientation},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a get properties request
		/// </summary>
		public static void GetProperties(MmoGuid guid, PropertyFlags flags)
		{
			if (flags == PropertyFlags.None)
				return;

			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.GetProperties},
					{(byte) ParameterCode.ObjectId, (long) guid},
					{(byte) ParameterCode.Flags, (int) flags},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends an interact request
		/// </summary>
		public static void Interact(MmoGuid guid, InteractionFlags flags)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.Interact},
					{(byte) ParameterCode.ObjectId, (long) guid},
				};

			if (flags != InteractionFlags.None)
				parameters.Add((byte)ParameterCode.Flags, (byte)flags);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a close interaction request
		/// </summary>
		public static void CloseInteraction()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.CloseInteraction},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a purchase item request
		/// </summary>
		public static void PurchaseItem(byte index, byte count)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.PurchaseItem},
					{(byte) ParameterCode.Data, new SlotItemStructure(index, count)},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sell item request
		/// </summary>
		public static void SellItem(byte index, byte count)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SellItem},
					{(byte) ParameterCode.Data, new SlotItemStructure(index, count)},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a loot item request
		/// </summary>
		public static void LootItem(byte index)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LootItem},
					{(byte) ParameterCode.Index, index},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a loot gold request
		/// </summary>
		public static void LootGold()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LootGold},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a loot all request
		/// </summary>
		public static void LootAll()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LootAll},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a select dialogue request
		/// </summary>
		public static void SelectDialogue(short dialogueId)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SelectDialogue},
					{(byte) ParameterCode.DialogueId, dialogueId},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a accept quest request
		/// </summary>
		public static void AcceptQuest(short questId)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.AcceptQuest},
					{(byte) ParameterCode.QuestId, questId},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a turn in quest request
		/// </summary>
		public static void TurnInQuest(short questId)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.TurnInQuest},
					{(byte) ParameterCode.QuestId, questId},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a use inventory item request
		/// </summary>
		public static void UseInventoryItem(byte slot, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.UseInventorySlot},
					{(byte) ParameterCode.Index, slot},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, (long)targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a move inventory item request
		/// </summary>
		public static void MoveInventoryItem(byte slotTo, byte slotFrom)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.MoveInventorySlot},
					{(byte) ParameterCode.Index, slotTo},
					{(byte) ParameterCode.IndexFrom, slotFrom},
				};
			
			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends an add actionbar item request
		/// </summary>
		public static void AddActionBarItem(byte slot, short itemId)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.AddActionbarItem},
					{(byte) ParameterCode.Index, slot},
					{(byte) ParameterCode.ItemId, itemId},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends an add actionbar spell request
		/// </summary>
		public static void AddActionBarSpell(byte slot, short spellId)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.AddActionbarSpell},
					{(byte) ParameterCode.Index, slot},
					{(byte) ParameterCode.SpellId, spellId},
				};

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}
		
		/// <summary>
		/// Sends a use action bar slot request
		/// </summary>
		public static void UseActionbarSlot(byte slot, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.UseActionbarSlot},
					{(byte) ParameterCode.Index, slot},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, (long)targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a cast spell request
		/// </summary>
		public static void CastSpell(short spellId, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.CastSpell},
					{(byte) ParameterCode.SpellId, spellId},
				};

			if (targetGuid != null)
				parameters.Add((byte) ParameterCode.ObjectId, (long) targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a chat message request
		/// </summary>
		public static void SendChat(MessageType messageType, string message, string receiver)
		{
			if(string.IsNullOrEmpty(message))
				return;

			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SendChat},
					{(byte) ParameterCode.MessageType, (byte) messageType},
					{(byte) ParameterCode.Message, message},
				};

			if (!string.IsNullOrEmpty(receiver))
				parameters.Add((byte)ParameterCode.Receiver, receiver);

			_engine.SendOperation(ClientOperationCode.Chat, parameters);
		}

		/// <summary>
		/// Sends a send friend request request
		/// </summary>
		public static void SendFriendRequest(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SendFriendRequest},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Social, parameters);
		}

		/// <summary>
		/// Sends an accept friend request request
		/// </summary>
		public static void AcceptFriendRequest(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.AcceptFriendRequest},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Social, parameters);
		}

		/// <summary>
		/// Sends a decline friend request request
		/// </summary>
		public static void DeclineFriendRequest(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.DeclineFriendRequest},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Social, parameters);
		}

		/// <summary>
		/// Sends a remove friend request
		/// </summary>
		public static void RemoveFriend(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.RemoveFriend},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Social, parameters);
		}

		/// <summary>
		/// Sends a send group invite request
		/// </summary>
		public static void SendGroupInvite(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SendGroupInvite},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
		}

		/// <summary>
		/// Sends an accept group invite request
		/// </summary>
		public static void AcceptGroupInvite()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.AcceptGroupInvite},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
		}

		/// <summary>
		/// Sends a decline group invite request
		/// </summary>
		public static void DeclineGroupInvite()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.DeclineGroupInvite},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
		}

		/// <summary>
		/// Sends a kick group member request
		/// </summary>
		public static void KickGroupMember(string name)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.KickGroupMember},
					{(byte) ParameterCode.CharacterName, name},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
			// can use id instead of char name?
			throw new NotImplementedException();
		}

		/// <summary>
		/// Sends a leave group request
		/// </summary>
		public static void LeaveGroup()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.LeaveGroup},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
		}

		/// <summary>
		/// Sends a disband group request
		/// </summary>
		public static void DisbandGroup()
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.DisbandGroup},
				};

			_engine.SendOperation(ClientOperationCode.Group, parameters);
		}

		/// <summary>
		/// Sends a sp heal request
		/// </summary>
		public static void SpHeal(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpHeal},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte) ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp damage request
		/// </summary>
		public static void SpDamage(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpDamage},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp add power request
		/// </summary>
		public static void SpAddPower(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpAddPower},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp remove power request
		/// </summary>
		public static void SpRemovePower(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpRemovePower},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp kill request
		/// </summary>
		public static void SpKill(MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpKill},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp add xp request
		/// </summary>
		public static void SpAddXp(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpAddXp},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp add spell request
		/// </summary>
		public static void SpAddSpell(short spellId, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpAddSpell},
					{(byte) ParameterCode.SpellId, spellId},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp remove spell request
		/// </summary>
		public static void SpRemoveSpell(short spellId, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpRemoveSpell},
					{(byte) ParameterCode.SpellId, spellId},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp add gold request
		/// </summary>
		public static void SpAddGold(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpAddGold},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}

		/// <summary>
		/// Sends a sp remove gold request
		/// </summary>
		public static void SpRemoveGold(int value, MmoGuid? targetGuid)
		{
			var parameters = new Dictionary<byte, object>
				{
					{1, (byte) GameOperationCode.SpRemoveGold},
					{(byte) ParameterCode.Value, value},
				};

			if (targetGuid != null)
				parameters.Add((byte)ParameterCode.ObjectId, targetGuid.Value);

			_engine.SendOperation(ClientOperationCode.World, parameters);
		}
	}
}