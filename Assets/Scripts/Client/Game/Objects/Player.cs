﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Client.Game.Items;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Client.Game.Objects
{
	public class Player : Character
	{
		#region Constants and Fields

		private readonly Inventory inventory;
		private readonly SpellManager spellManager;
		private readonly Actionbar actionBar;
		private readonly QuestManager questManager;
		private readonly SocialManager socialManager;
		
		private readonly short[] baseStats;
		private readonly Origin origin;

		private int xp;
		private int maxXp;
		private int money;

		private readonly List<Character> visibleEnemies;

		private MmoObject currentInteractor;
		private MmoObject currentTarget;

		private bool canUseItem;
		
		private int targetCounter;

		// movement stuff
		private MovementKeys lastKeys;
		private float lastRotation;

		private const float ROTATION_CHECK_INTERVAL = 0.1f;
		private float nextRotationCheck;

		private const float MOVEMENT_UPDATE_INTERVAL = 0.05f;
		private float nextMovementUpdate;

		private Vector3 lerpFactor;
		private bool lerp;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="Inventory"/>.
		/// </summary>
		public Inventory Inventory
		{
			get
			{
				return inventory;
			}
		}

		/// <summary>
		/// Gets the actionbar
		/// </summary>
		public Actionbar Actionbar
		{
			get
			{
				return this.actionBar;
			}
		}

		/// <summary>
		/// Gets the <see cref="SpellManager"/>
		/// </summary>
		public SpellManager SpellManager
		{
			get
			{
				return this.spellManager;
			}
		}

		/// <summary>
		/// Gets the <see cref="QuestManager"/>.
		/// </summary>
		public QuestManager QuestManager
		{
			get
			{
				return this.questManager;
			}
		}

		/// <summary>
		/// Gets the <see cref="SocialManager"/>.
		/// </summary>
		public SocialManager SocialManager
		{
			get
			{
				return this.socialManager;
			}
		}

		/// <summary>
		/// Current player experience points
		/// </summary>
		public int Xp
		{
			get
			{
				return this.xp;
			}
		}

		/// <summary>
		/// Player's available fund
		/// </summary>
		public int Money
		{
			get
			{
				return this.money;
			}
		}

		/// <summary>
		/// Gets the max xp
		/// </summary>
		public int MaxXp
		{
			get
			{
				return this.maxXp;
			}
		}

		/// <summary>
		/// Set this to indicate whether the player can use any item
		/// </summary>
		public bool CanUseItem
		{
			get
			{
				return this.canUseItem && !this.IsDead;
			}
			set
			{
				this.canUseItem = value;
			}
		}

		/// <summary>
		/// Tells whether the player is in group or not
		/// </summary>
		public bool InGroup
		{
			get
			{
				return this.Group != null && this.Group.Count > 0;
			}
		}

		/// <summary>
		/// Gets or sets the group
		/// </summary>
		public Group Group { get; set; }

		/// <summary>
		/// Gets the current interactor
		/// </summary>
		public MmoObject CurrentInteractor
		{
			get
			{
				return this.currentInteractor;
			}
		}

		/// <summary>
		/// Gets the current target
		/// </summary>
		public MmoObject CurrentTarget
		{
			get
			{
				return this.currentTarget;
			}
		}

		/// <summary>
		/// Called when the <see cref="Player"/>'s focus has been set.
		/// </summary>
		public event Action<MmoObject> OnFocusSet;

		#endregion

		#region Constructors and Destructors

		public Player(MmoWorld zone, MmoGuid id, IAvatar avatar)
			: base(zone, id, avatar)
		{
			this.inventory = new Inventory(GlobalGameSettings.PLAYER_DEFAULT_INVENTORY_SIZE, itemData => new MmoItem(itemData));
			this.actionBar = new Actionbar(GlobalGameSettings.PLAYER_ACTION_BAR_SIZE);
			this.spellManager = new SpellManager(this);
			this.questManager = new QuestManager();
			this.socialManager = new SocialManager();

			this.baseStats = new short[Stats.eLastStat - Stats.eFirstStat + 1];
			this.visibleEnemies = new List<Character>();

			this.canUseItem = true;
			this.nextRotationCheck = 0;
		}

		#endregion

		#region Game System Methods

		/// <summary>
		/// Called when the <see cref="Player"/> is initialized
		/// </summary>
		protected override void OnInitialize()
		{
			base.OnInitialize();
			
			World.Game.AddHotkey(KeyCode.Escape, OnFocusResetHotkey);
			World.Game.AddHotkey(KeyCode.Tab, OnSwitchFocusHotkey);
			World.Game.AddHotkey(KeyCode.I, HUD.Instance.ToggleInventory);
			World.Game.AddHotkey(KeyCode.C, WindowManager.Instance.ToggleCharacterWindow);
			World.Game.AddHotkey(KeyCode.J, WindowManager.Instance.ToggleJournalWindow);

			World.Game.AddHotkey(KeyCode.Alpha1, () => this.OnActionBarHotkey(0));
			World.Game.AddHotkey(KeyCode.Alpha2, () => this.OnActionBarHotkey(1));
			World.Game.AddHotkey(KeyCode.Alpha3, () => this.OnActionBarHotkey(2));
			World.Game.AddHotkey(KeyCode.Alpha4, () => this.OnActionBarHotkey(3));
			World.Game.AddHotkey(KeyCode.Alpha5, () => this.OnActionBarHotkey(4));
			World.Game.AddHotkey(KeyCode.Alpha6, () => this.OnActionBarHotkey(5));
			World.Game.AddHotkey(KeyCode.Alpha7, () => this.OnActionBarHotkey(6));
			World.Game.AddHotkey(KeyCode.Alpha8, () => this.OnActionBarHotkey(7));
			World.Game.AddHotkey(KeyCode.Alpha9, () => this.OnActionBarHotkey(8));
			World.Game.AddHotkey(KeyCode.Alpha0, () => this.OnActionBarHotkey(9));
		}

		/// <summary>
		/// Called when the <see cref="Player"/> is sapwned
		/// </summary>
		protected override void OnSpawn(Vector3 position, Vector3 rotation)
		{
			this.SetPosition(position, false);
			this.SetRotation(rotation, true);
			this.Avatar.Spawn(Position);

			this.SetInteractability(false);

			ThirdPersonController.Instance.CanControl = true;
			ThirdPersonController.Instance.Orientation = this.Rotation.y;

			this.lastKeys = ThirdPersonController.Instance.Keys;
			this.lastRotation = ThirdPersonController.Instance.Orientation;
		}

		/// <summary>
		/// Called during update
		/// </summary>
		protected override void OnUpdate()
		{
			var tpc = ThirdPersonController.Instance;
			if (tpc.Keys != lastKeys)
			{
				var sendKeys = true;
				if (tpc.Keys == MovementKeys.None)
				{
					// last key was only jump so no need to send stop
					if ((lastKeys & ~MovementKeys.Jump) == MovementKeys.None)
						sendKeys = false;

					// last key was only modifiers so no need to send stop
					if ((lastKeys & ~MovementKeys.ModifierKeys) == MovementKeys.None)
						sendKeys = false;
				}
				else
				{
					// only modifiers
					if ((tpc.Keys & ~MovementKeys.ModifierKeys) == MovementKeys.None)
						sendKeys = false;

					// last key had jump, the current key doesn't have jump, but the last key was the same now with jump omitted
					if ((lastKeys & MovementKeys.Jump) == MovementKeys.Jump && (tpc.Keys & MovementKeys.Jump) != MovementKeys.Jump &&
					    (lastKeys & ~MovementKeys.Jump) == tpc.Keys)
						sendKeys = false;
				}
				
				if (sendKeys)
					Operations.Move(tpc.Keys);

				this.lastKeys = tpc.Keys;
			}

			if (lerp && tpc.Speed > 0)
			{
				var newPosition = tpc.transform.position;
				newPosition = newPosition + (lerpFactor / tpc.Speed) * Time.deltaTime;
				tpc.transform.position = newPosition;

				newPosition = this.World.GetInterpolatedPosition(newPosition);
				this.SetPosition(newPosition, true);
			}

			if (Time.time > this.nextRotationCheck)
			{
				if (ThirdPersonController.Instance.CanRotateCharacter)
				{
					var newOrientation = Camera.main.transform.eulerAngles.y;
					if (Math.Abs(newOrientation - this.lastRotation) > 0.1f)
					{
						// set rotation
						Operations.Rotate(newOrientation);
						this.lastRotation = newOrientation;
					}
				}

				this.nextRotationCheck = Time.time + ROTATION_CHECK_INTERVAL;
			}

			if (Time.time > this.nextMovementUpdate)
			{
				var newPosition = tpc.transform.position;
				newPosition = this.World.GetInterpolatedPosition(newPosition);
				this.SetPosition(newPosition, true);

				this.nextMovementUpdate = Time.time + MOVEMENT_UPDATE_INTERVAL;
			}

			if (Math.Abs(tpc.Orientation - this.lastRotation) > 0.1f)
			{
				var newRotation = this.Rotation;
				newRotation.y = Mathf.LerpAngle(tpc.Orientation, this.lastRotation, Time.deltaTime * GameSettings.PLAYER_SMOOTH_ORIENTATION_FACTOR);
				tpc.Orientation = newRotation.y;
				this.SetRotation(newRotation, false);
			}

			// making sure that we are staying within range of our interactor
			if(currentInteractor != null)
			{
				// are we within the interaction range?
				const float minInteractionRange = GlobalGameSettings.MIN_INTERACTION_RANGE;
				if (Position.GetDistanceSquared(currentInteractor.Position) > minInteractionRange * minInteractionRange)
					this.CloseInteraction();
			}

			this.spellManager.Update(Time.deltaTime);
		}

		/// <summary>
		/// Called when the <see cref="Player"/> is destroyed
		/// </summary>
		protected override void OnDestroy()
		{
			this.Kill();
		}

		#endregion

		#region Property System Implementation

		/// <summary>
		/// Gets all the base stats
		/// </summary>
		public IEnumerable<short> GetBaseStats()
		{
			return this.baseStats;
		}

		/// <summary>
		/// Gets the value of a certain <see cref="Stats"/>.
		/// </summary>
		public short GetBaseStat(Stats stat)
		{
			var index = stat - Stats.eFirstStat;
			if (index >= baseStats.Length)
				return 0;

			return this.baseStats[index];
		}

		/// <summary>
		/// Sets the value of a certain <see cref="Stats"/>.
		/// </summary>
		public short SetBaseStat(Stats stat, short value)
		{
			if (value < 0)
				value = 0;

			var index = stat - Stats.eFirstStat;
			if (index >= baseStats.Length)
				return 0;

			this.baseStats[index] = value;
			return value;
		}

		/// <summary>
		/// Calculates the resultant value of a <see cref="Stats"/>.
		/// </summary>
		public short CalculateStat(Stats stat)
		{
			return StatsHelper.GetCharacterStatCalculator(stat).CalculateValue(this, stat);
		}

		/// <summary>
		/// Called when a property has been set in the <see cref="System.Collections.Hashtable"/>
		/// </summary>
		protected override void OnPropertySet(PropertyCode propertiesCode, object data)
		{
			switch (propertiesCode)
			{
				case PropertyCode.Level:
					base.OnPropertySet(propertiesCode, data);
					this.SetMaxXp(Formulas.GetMaxXp(Level));
					break;

				case PropertyCode.Xp:
					this.SetXp((int) data);
					break;

				case PropertyCode.Money:
					this.SetMoney((int) data);
					break;

				case PropertyCode.Stats:
					var newStats = (short[]) data;
					for (var i = Stats.eFirstStat; i <= Stats.eLastStat; i++)
						this.SetBaseStat(i, newStats[i - Stats.eFirstStat]);
					break;

				default:
					base.OnPropertySet(propertiesCode, data);
					break;
			}
		}

		/// <summary>
		/// Sets the xp
		/// </summary>
		public void SetXp(int amount)
		{
			this.xp = amount;
			this.CharInfo.XpInfo = string.Format("{0} / {1}", this.Xp, this.MaxXp);
		}

		/// <summary>
		/// Sets the max xp
		/// </summary>
		public void SetMaxXp(int amount)
		{
			this.maxXp = amount;
		}

		/// <summary>
		/// Sets Noney
		/// </summary>
		public void SetMoney(int amount)
		{
			this.money = amount;
		}

		/// <summary>
		/// Increases the level by one.
		/// Sets the max xp using <see cref="Formulas.GetMaxXp"/>.
		/// Resets the max and current hp and power.
		/// </summary>
		public void LevelUp()
		{
			if (Level == GlobalGameSettings.MAX_PLAYER_LEVEL)
				return;
			
			this.SetLevel((byte)(Level + 1));
			this.SetMaxXp(Formulas.GetMaxXp(Level));

			this.SetMaxHealth(CalculateStat(Stats.Health));
			this.SetMaxPower(CalculateStat(Stats.Health));
			
			this.SetHealth(MaximumHealth);
			this.SetPower(MaximumPower);
		}

		#endregion

		#region Interaction System Implementation

		/// <summary>
		/// Validates the interaction and starts interacting with the <see cref="MmoObject"/>.
		/// </summary>
		/// <param name="mmoObject"></param>
		public void TryInteract(MmoObject mmoObject)
		{
			// cannot interact with ourselves
			if (mmoObject == this)
				return;

			// are we within the interaction range?
			const float minInteractionRange = GlobalGameSettings.MIN_INTERACTION_RANGE;
			if (Position.GetDistanceSquared(mmoObject.Position) > minInteractionRange * minInteractionRange)
				return;

			switch ((ObjectType) mmoObject.Guid.Type)
			{
				case ObjectType.Player:
					{
						// setting our target
						this.SetTarget(mmoObject);
						// cannot interact with other players for now
					}
					break;

				case ObjectType.Npc:
					{
						// no flags means we cannot interact (more like we do not need to because its unnecessary)
						if (mmoObject.Flags == InterestFlags.None)
							return;

						var npc = (Npc) mmoObject;
						var requestFlags = InteractionFlags.None;

						// if the npc has the loot flag,
						if (npc.HasFlag(InterestFlags.Loot))
						{
							// and it does not have any loot, we need to request the server to send all loot items
							// this can happen when the object is being interacted for the first time
							if (!npc.HaveLoot())
								requestFlags |= InteractionFlags.SendLoot;
						}

						// if the npc has the shopping flag,
						if(npc.HasFlag(InterestFlags.Shopping))
						{
							// and it does not have any items, we need to request the server to send the inventory
							// this can happen when the npc is being interacted for the first time
							if(npc.Inventory == null)
								requestFlags |= InteractionFlags.SendInventory;
						}

						// setting our target
						this.SetTarget(mmoObject);
						// setting our interactor
						this.currentInteractor = mmoObject;
						Operations.Interact(mmoObject.Guid, requestFlags);
					}
					break;

				case ObjectType.Gameobject:
					{
						// no flags means we cannot interact (more like we do not need to because its unnecessary)
						if (mmoObject.Flags == InterestFlags.None)
							return;

						var requestFlags = InteractionFlags.None;
						// if the object has the loot flag,
						if (mmoObject.HasFlag(InterestFlags.Loot))
						{
							// and it does not have any loot, we need to request the server to send all loot items
							// this can happen when the object is being interacted for the first time
							if (!mmoObject.HaveLoot())
								requestFlags |= InteractionFlags.SendLoot;
						}

						// setting our target
						this.SetTarget(mmoObject);
						// setting our interactor
						this.currentInteractor = mmoObject;
						Operations.Interact(mmoObject.Guid, requestFlags);
					}
					break;
			}
		}

		/// <summary>
		/// Closes the current interaction
		/// </summary>
		public void CloseInteraction()
		{
			if(currentInteractor == null)
				return;

			// notifying the server
			Operations.CloseInteraction();
			// close the interaction window if its open
			WindowManager.Instance.CloseInteractionWindow();
			// finally resetting our interactor
			this.currentInteractor = null;
		}

		/// <summary>
		/// Validates the target and sets the current target
		/// </summary>
		/// <param name="mmoObject"></param>
		public void TryTarget(MmoObject mmoObject)
		{
			// if we are trying to target the same target again, just return
			if(currentTarget == mmoObject)
				return;

			switch ((ObjectType)mmoObject.Guid.Type)
			{
				case ObjectType.Player:
				case ObjectType.Npc:
					this.SetTarget(mmoObject);
					break;
			}
		}

		/// <summary>
		/// Closes the current target
		/// </summary>
		public void CloseTarget()
		{
			this.SetTarget(null);
		}

		/// <summary>
		/// Sets the current target
		/// </summary>
		/// <param name="newTarget"></param>
		void SetTarget(MmoObject newTarget)
		{
			if(currentTarget == newTarget)
				return;

			// only players and npc's can be targetted
			var oldTarget = this.currentTarget;
			if (oldTarget != null)
				oldTarget.OnPlayerUnfocus();

			this.currentTarget = newTarget;
			if (OnFocusSet != null)
				this.OnFocusSet(newTarget);

			if(newTarget != null)
				newTarget.OnPlayerFocus();
		}

		/// <summary>
		/// Adds a visible enemy for targetting
		/// </summary>
		public void AddVisibleEnemy(Character character)
		{
			this.visibleEnemies.Add(character);
		}

		/// <summary>
		/// Removes a visible enemy from the list
		/// </summary>
		public void RemoveVisibleEnemy(Character character)
		{
			if (visibleEnemies.Contains(character))
			{
				if (targetCounter >= visibleEnemies.IndexOf(character))
				{
					this.targetCounter = Mathf.Clamp(this.targetCounter - 1, 0, visibleEnemies.Count - 2);
				}

				this.visibleEnemies.Remove(character);
			}
		}

		void OnFocusResetHotkey()
		{
			if (currentInteractor != null)
			{
				this.CloseInteraction();
				return;
			}

			if (currentTarget != null)
			{
				this.CloseTarget();
				return;
			}

			WindowManager.Instance.ToggleOptionsWindow();
		}

		void OnSwitchFocusHotkey()
		{
			if (visibleEnemies.Count > 0)
			{
				this.visibleEnemies.Sort((t1, t2) => this.Position.GetDistanceSquared(t1.Position).CompareTo(this.Position.GetDistanceSquared(t2.Position)));

				var target = this.visibleEnemies[this.targetCounter];
				if (Position.GetDistance(target.Position) > GameSettings.PLAYER_TARGET_MIN_DISTANCE)
				{
					if (targetCounter == 0)
						return;
					this.targetCounter = 0;
				}

				this.SetTarget(target);
				this.targetCounter++;

				if (targetCounter >= this.visibleEnemies.Count)
					this.targetCounter = 0;
			}
		}

		#endregion

		#region Movement System Implementation

		/// <summary>
		/// Smoothly moves to a position
		/// </summary>
		public sealed override void Move(Vector3 newPosition)
		{
			var distance = this.Position.GetDistance(newPosition);
			if (distance > GameSettings.MIN_INSTANT_SNAP_DISTANCE)
			{
				newPosition = this.World.GetInterpolatedPosition(newPosition);
				this.SetPosition(newPosition, true);

				this.lerpFactor = Vector3.zero;
				this.lerp = false;

				return;
			}

			if (distance <= GameSettings.MIN_IGNORE_LERP_DISTANCE)
			{
				this.lerpFactor = Vector3.zero;
				this.lerp = false;

				return;
			}

			this.lerpFactor = (newPosition - this.Position).normalized;
			this.lerp = true;
		}

		/// <summary>
		/// Sets the movement direction
		/// </summary>
		public sealed override void SetDirection(MovementDirection value)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Sets the movement speed
		/// </summary>
		public sealed override void SetSpeed(byte value)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Sets the movement state
		/// </summary>
		public sealed override void SetMovementState(MovementState value)
		{
			throw new NotImplementedException();
		}

		#endregion

		#region Callback.OnActionBarHotkey

		/// <summary>
		/// Called when an action bar key has been pressed
		/// </summary>
		/// <param name="index"></param>
		void OnActionBarHotkey(int index)
		{
			Actionbar.ActionItem actionItem;
			if(!actionBar.TryGetItem(index, out actionItem))
				return;

			Operations.UseActionbarSlot((byte) index, currentTarget != null ? currentTarget.Guid : (MmoGuid?) null);
		}

		#endregion
	};
}
