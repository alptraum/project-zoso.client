﻿using System;

using UnityEngine;

using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Objects;

public class CharacterAvatar : Avatar
{
	#region Constants and Fields

	private Character characterOwner;
	private Vector3 lerpPosition;

	protected Projector FocusRing;

	private Transform center;
	private Transform lootEffectInstance;

	private bool isFading;
	private bool fadeIn = true;

	/// <summary>
	/// overhead indicator object
	/// </summary>
	private GameObject overheadIndicator;
	private bool rotateOverheadIndicator;

	private CharacterAnimationState animationState;

	#endregion

	#region Properties

	/// <summary>
	/// Gets the center
	/// </summary>
	public override Vector3 Center
	{
		get
		{
			return center.position;
		}
	}

	/// <summary>
	/// Gets the height
	/// </summary>
	public override float Height
	{
		get
		{
			return GetComponent<Collider>().bounds.extents.y * 2;
		}
	}

	/// <summary>
	/// Tells whether the Owner is the current target or not
	/// </summary>
	protected bool IsCurrentTarget { get; private set; }
	
	#endregion

	#region Avatar Methods Overrides

	/// <summary>
	/// Called on Awake
	/// </summary>
	protected override void OnAwake()
	{
		this.FocusRing = this.GetComponentInChildren<Projector>();
		this.FocusRing.enabled = false;

		this.center = this.transform.Find("center");

		var matColor = this.ObjectRenderer.material.color;
		matColor.a = 0;
		this.ObjectRenderer.material.color = matColor;

		this.isFading = true;
	}

	/// <summary>
	/// Called on Update
	/// </summary>
	protected override void OnUpdate()
	{
		if (isFading)
		{
			this.HandleFading();
		}

		if (InView)
		{
			this.HandleAnimation();
		}
	}

	/// <summary>
	/// Called when destroyed
	/// </summary>
	protected override void OnAvatarDestroy()
	{
		this.isFading = true;
		this.fadeIn = false;

		Destroy(FocusRing);
	}

	/// <summary>
	/// Called after setting the owner
	/// </summary>
	/// <param name="newOwner"></param>
	protected override void OnSetOwner(MmoObject newOwner)
	{
		var owner = newOwner as Character;
		if (owner == null)
			throw new InvalidCastException();

		this.characterOwner = owner;
		this.AvatarName = newOwner.Name; // this.name != this.Name; remember is GameObject.name
	}

	/// <summary>
	/// Called when the player focuses on this <see cref="Avatar"/>.
	/// </summary>
	protected override void OnPlayerFocus()
	{
		if (FocusRing != null)
		{
			this.FocusRing.enabled = true;
			this.IsCurrentTarget = true;
		}
	}

	/// <summary>
	/// Called when the player loses focuses from this <see cref="Avatar"/>.
	/// </summary>
	protected override void OnPlayerUnfocus()
	{
		if (FocusRing != null)
		{
			this.FocusRing.enabled = false;
			this.IsCurrentTarget = false;
		}
	}

	/// <summary>
	/// Called when the state or direction is changed
	/// </summary>
	protected override void UpdateAnimationState()
	{
		var lAnimState = CharacterAnimationState.Idle;
		switch (State)
		{
			case MovementState.Idle:
				break;

			case MovementState.Running:
				{
					if ((Direction & MovementDirection.Forward) == MovementDirection.Forward)
					{
						lAnimState = CharacterAnimationState.Running;
					}
					else if ((Direction & MovementDirection.Backward) == MovementDirection.Backward)
					{
						lAnimState = CharacterAnimationState.Backing;
					}
					else if ((Direction & MovementDirection.Left) == MovementDirection.Left)
					{
						lAnimState = CharacterAnimationState.StrafingLeft;
					}
					else if ((Direction & MovementDirection.Right) == MovementDirection.Right)
					{
						lAnimState = CharacterAnimationState.StrafingRight;
					}
				}
				break;

			default:
				{
					Logger.WarningFormat("Animation ({0} | {1}) is not handled", this.State, this.Direction);
				}
				break;
		}

		this.animationState = lAnimState;
	}

	/// <summary>
	/// Destroys all created <see cref="GameObject"/>.
	/// </summary>
	protected override void DestroyGameObject()
	{
		if (IsCurrentTarget)
		{
			this.characterOwner.World.Player.CloseTarget();
		}

		if (ShowTooltip)
		{
			TooltipManager.CloseTooltip();
		}

		Destroy(gameObject);
	}

	/// <summary>
	/// Called on mouse over
	/// </summary>
	protected override void OnBeginHighlight()
	{
		if (!isFading)
		{
			HUD.SetCursor(GetInteractionCursor());
			ObjectHighligher.OnHover(this.ObjectRenderer);
		}

		if (ShowTooltip == false && !IsCurrentTarget)
		{
			HUD.BeginNameplate(characterOwner, HUD.GetNameplateColor(characterOwner));
			this.ShowTooltip = true;
		}
	}

	/// <summary>
	/// Gets the cursor for a certain <see cref="MmoObject"/>.
	/// </summary>
	/// <returns></returns>
	CursorType GetInteractionCursor()
	{
		if (characterOwner.HasFlag(InterestFlags.Loot))
			return CursorType.CURSOR_USE;

		if (characterOwner.HasFlag(InterestFlags.Quest))
			return CursorType.CURSOR_TALK;

		if (characterOwner.HasFlag(InterestFlags.Shopping))
			return CursorType.CURSOR_TALK;

		if (characterOwner.HasFlag(InterestFlags.Conversation))
			return CursorType.CURSOR_TALK;

		return CursorType.CURSOR_DEFAULT;
	}

	/// <summary>
	/// Called on mouse leave
	/// </summary>
	protected override void OnEndHighlight()
	{
		HUD.SetCursor(CursorType.CURSOR_DEFAULT);

		if (!isFading)
		{
			ObjectHighligher.OnDehover(this.ObjectRenderer);
		}

		if (ShowTooltip)
		{
			HUD.EndNameplate();
			this.ShowTooltip = false;
		}
	}

	#endregion

	#region Local Methods

	private void HandleFading()
	{
		var matColor = this.ObjectRenderer.material.color;

		if (fadeIn)
		{
			matColor.a += Time.deltaTime / 2;

			if (matColor.a >= 1)
			{
				this.isFading = false;
			}
		}
		else
		{
			matColor.a -= Time.deltaTime / 2;

			if (matColor.a <= 0)
			{
				this.isFading = false;
				this.DestroyGameObject();
			}
		}

		this.ObjectRenderer.material.color = matColor;
	}

	private void HandleAnimation()
	{
		switch (this.animationState)
		{
			case CharacterAnimationState.Idle:
				{
					this.GetComponent<Animation>().CrossFade(GameSettings.IDLE_ANIMATION_NAME);
				}
				break;

			case CharacterAnimationState.Running:
				{
					this.GetComponent<Animation>().CrossFade(GameSettings.RUN_ANIMATION_NAME);
				}
				break;

			case CharacterAnimationState.Backing:
				{
					this.GetComponent<Animation>()[GameSettings.BACK_ANIMATION_NAME].speed = Math.Abs(this.GetComponent<Animation>()[GameSettings.BACK_ANIMATION_NAME].speed) * -1;
					this.GetComponent<Animation>().CrossFade(GameSettings.BACK_ANIMATION_NAME);
				}
				break;

			case CharacterAnimationState.StrafingLeft:
				{
					this.GetComponent<Animation>().CrossFade(GameSettings.STRAFE_LEFT_ANIMATION_NAME);
				}
				break;

			case CharacterAnimationState.StrafingRight:
				{
					this.GetComponent<Animation>().CrossFade(GameSettings.STRAFE_RIGHT_ANIMATION_NAME);
				}
				break;
		}
	}

	#endregion
}
