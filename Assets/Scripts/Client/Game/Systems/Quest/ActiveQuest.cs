﻿using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class ActiveQuest
	{
		private readonly short questId;

		public ActiveQuest(short questId, QuestStatus status, byte[] counts)
		{
			this.questId = questId;
			this.Counts = counts;
			this.Status = status;
		}

		/// <summary>
		/// Gets the quest id
		/// </summary>
		public short QuestId
		{
			get
			{
				return questId;
			}
		}

		/// <summary>
		/// Gets or Sets the status
		/// </summary>
		public QuestStatus Status { get; set; }

		/// <summary>
		/// Gets or sets the current objective counts
		/// </summary>
		public byte[] Counts { get; set; }
	}
}
