﻿using System;
using System.Linq;
using System.Collections.Generic;

using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class Actionbar
	{
		#region Constants and Fields

		public struct ActionItem
		{
			public ActionItemType Type;
			public short ItemId;
		}

		struct Slot : ISlotView<ActionItem>
		{
			#region Implementation of ISlotView<out ActionItem>

			/// <summary>
			/// Gets the item
			/// </summary>
			public ActionItem Item { get; set; }

			/// <summary>
			/// Determines whether the slot is empty or not
			/// </summary>
			public bool IsEmpty { get; set; }

			#endregion
		}

		/// <summary>
		/// the action item slots
		/// </summary>
		private readonly Slot[] slots;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the container size
		/// </summary>
		public int Size
		{
			get
			{
				return slots.Length;
			}
		}

		/// <summary>
		/// Gets the filled slots count
		/// </summary>
		public int FilledSlots { get; private set; }

		/// <summary>
		/// Tells whether the container is empty or not
		/// </summary>
		public bool IsEmpty
		{
			get
			{
				return this.FilledSlots == 0;
			}
		}

		/// <summary>
		/// Tells whether the inventory is full or not(only counting slots)
		/// </summary>
		public bool IsFull
		{
			get
			{
				return this.FilledSlots == this.Size;
			}
		}

		#endregion

		#region Constructor and Destructor

		/// <summary>
		/// Creates a new instance of the <see cref="Actionbar"/> class.
		/// </summary>
		public Actionbar(int size)
		{
			this.slots = new Slot[size];
			for (var i = 0; i < slots.Length; i++)
				this.slots[i].IsEmpty = true;

			this.FilledSlots = 0;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Adds an item at the next empty slot
		/// </summary>
		public bool AddItem(ActionItemType itemType, short itemId)
		{
			// try to find an empty slot
			for (var i = 0; i < this.Size; i++)
			{
				// if the slot isnt empty skip
				if (!slots[i].IsEmpty)
					continue;
				
				// add the item at an empty slot
				this.slots[i] = new Slot { Item = new ActionItem { Type = itemType, ItemId = itemId }, IsEmpty = false };
				this.FilledSlots++;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Adds an item at a slot
		/// </summary>
		public bool AddItemAt(ActionItemType itemType, short itemId, int slot)
		{
			// range check
			if (slot < 0 || slot >= this.Size)
				return false;
			
			// add the item at that slot
			this.slots[slot] = new Slot { Item = new ActionItem { Type = itemType, ItemId = itemId }, IsEmpty = false };
			this.FilledSlots++;
			return true;
		}

		/// <summary>
		/// Removes a specific item
		/// </summary>
		public bool RemoveItem(ActionItemType itemType, short itemId)
		{
			for (var i = 0; i < this.Size; i++)
			{
				var slot = slots[i];
				if (slot.IsEmpty)
					continue;

				var item = slot.Item;
				if (item.Type != itemType || item.ItemId != itemId)
					continue;

				this.slots[i].IsEmpty = true;
				this.FilledSlots--;
				return true;
			}

			return false;
		}

		/// <summary>
		/// Removes an item at a specific slot
		/// </summary>
		public bool RemoveItemAt(int slot)
		{
			// range check
			if (slot < 0 || slot >= this.Size)
				return false;

			if (slots[slot].IsEmpty)
				return false;

			this.slots[slot].IsEmpty = true;
			this.FilledSlots--;
			return true;
		}

		/// <summary>
		/// Moves an item from an index to another within this <see cref="Actionbar"/>
		/// </summary>
		public bool MoveItem(int indexTo, int indexFrom)
		{
			return this.MoveItem(this, indexTo, indexFrom);
		}

		/// <summary>
		/// Moves an item from an <see cref="Actionbar"/> to this <see cref="Actionbar"/>
		/// </summary>
		public bool MoveItem(Actionbar actionBarFrom, int slotTo, int slotFrom)
		{
			// range check
			if (slotTo < 0 || slotTo >= this.Size || slotFrom < 0 || slotFrom >= actionBarFrom.Size)
				return false;

			// making sure that we are not moving an empty item
			if (actionBarFrom.slots[slotFrom].IsEmpty)
				return false;

			var itemTo = this.slots[slotTo];
			this.slots[slotTo] = actionBarFrom.slots[slotFrom];
			actionBarFrom.slots[slotFrom] = itemTo;
			return true;
		}

		/// <summary>
		/// Gets the <see cref="ActionItem"/> at a slot
		/// </summary>
		public bool TryGetItem(int slot, out ActionItem item)
		{
			item = default(ActionItem);
			// range check
			if (slot < 0 || slot >= this.Size)
				return false;

			var slotslot = this.slots[slot];
			if (slotslot.IsEmpty)
				return false;

			item = slotslot.Item;
			return true;
		}

		/// <summary>
		/// Tells whether the slot is empty or not
		/// </summary>
		public bool SlotEmpty(int slot)
		{
			if (slot < 0 || slot >= this.Size)
				throw new IndexOutOfRangeException("IndexOutOfRange");

			return slots[slot].IsEmpty;
		}

		/// <summary>
		/// Get all the slots
		/// </summary>
		public IEnumerable<ISlotView<ActionItem>> GetSlots()
		{
			return this.slots.Cast<ISlotView<ActionItem>>();
		}

		#endregion
	}
}
