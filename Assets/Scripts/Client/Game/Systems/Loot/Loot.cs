﻿namespace Karen90MmoFramework.Client.Game.Systems
{
	public class Loot
	{
		#region Constants and Fields

		private readonly LootItem[] items;
		private int gold;

		#endregion

		#region Constructors and Destructors

		/// <summary>
		/// Creates a new <see cref="Loot"/>.
		/// </summary>
		public Loot(int gold, LootItem[] items)
		{
			this.gold = gold;
			this.Count = 0;

			if (items != null)
			{
				this.items = items;
				this.Count = this.items.Length;
			}
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Tells whether the loot is empty or not
		/// </summary>
		public bool IsEmpty
		{
			get
			{
				return this.gold == 0 && this.Count == 0;
			}
		}

		/// <summary>
		/// Gets the available items
		/// </summary>
		public int Count { get; private set; }

		/// <summary>
		/// Gets the gold
		/// </summary>
		public int Gold
		{
			get
			{
				return this.gold;
			}
		}

		/// <summary>
		/// Get all the loots
		/// </summary>
		public LootItem[] GetLoots()
		{
			return this.items;
		}

		/// <summary>
		/// Collects the loot item at an index
		/// </summary>
		public void CollectLootItem(int index)
		{
			if (index >= this.items.Length)
				return;

			if (items[index].Status == LootItemStatus.Unlooted)
			{
				Operations.LootItem((byte) index);
				this.items[index].Status = LootItemStatus.Looting;
			}
		}

		/// <summary>
		/// Collects the gold
		/// </summary>
		public void CollectGold()
		{
			if (gold > 0)
			{
				Operations.LootGold();
				this.gold = 1 - this.gold;
			}
		}

		/// <summary>
		/// Collects all loot item
		/// </summary>
		public void CollectAll()
		{
			var looting = 0;
			if (gold > 0)
			{
				this.gold = 1 - this.gold;
				looting++;
			}

			if (items != null)
			{
				for (var i = 0; i < this.items.Length; i++)
				{
					if (items[i].Status == LootItemStatus.Unlooted)
					{
						this.items[i].Status = LootItemStatus.Looting;
						looting++;
					}
				}
			}

			if (looting > 0)
				Operations.LootAll();
		}

		/// <summary>
		/// Removes the loot item at an index
		/// </summary>
		public void RemoveLootItem(int index)
		{
			if (index >= this.items.Length)
				return;

			if (items[index].Status != LootItemStatus.Looted)
			{
				this.items[index].Status = LootItemStatus.Looted;
				this.Count--;
			}
		}

		/// <summary>
		/// Removes the gold
		/// </summary>
		public void RemoveGold()
		{
			if (gold == 0)
				return;

			this.gold = 0;
		}

		#endregion
	}
}
