﻿namespace Karen90MmoFramework.Client.Game
{
	public enum RadarVisibility : byte
	{
		Always = 0,
		WhenSeen = 1,
	};
}