﻿namespace Karen90MmoFramework.Client.Game
{
	public enum ContainerType : byte
	{
		Inventory,
		Actionbar,
		Equipment,
		PersonalBank,
		GuidBank,
	};
}