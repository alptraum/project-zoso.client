﻿namespace Karen90MmoFramework.Client.Game
{
	public enum TooltipStyle : byte
	{
		Button,
		Item,
		Character,
		Spell,
		GameObject,
	};
}