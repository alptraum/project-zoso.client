﻿namespace Karen90MmoFramework.Client.Game
{
	public enum UserLoginOperationType
	{
		None = 0,
		Login = 1,
		NewUser = 2,
	};
}