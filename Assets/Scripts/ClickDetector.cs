﻿using UnityEngine;

using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Client.Game;

public class ClickDetector
{
	#region Constants and Fields

	private Vector3 usePosition;
	private readonly LayerMask layerMask;

	#endregion

	#region Constructors and Destructors

	public ClickDetector(LayerMask layerMask)
	{
		this.layerMask = layerMask;
	}

	#endregion

	#region Public Methods
	
	/// <summary>
	/// Update Method
	/// </summary>
	public void Update()
	{
		if (HUD.winControl != 0)
			return;

		if (Input.GetMouseButtonDown(GameSettings.NPC_PRIMARY_ACTION_KEY) || Input.GetMouseButtonDown(GameSettings.NPC_SECONDARY_ACTION_KEY))
			usePosition = Input.mousePosition;

		if (usePosition == Input.mousePosition)
		{
			if (Input.GetMouseButtonUp(GameSettings.NPC_PRIMARY_ACTION_KEY))
			{
				var clickObject = this.getClickObject();

				if (clickObject != null)
					clickObject.SendMessage("OnInteract", SendMessageOptions.DontRequireReceiver);
			}

			if (Input.GetMouseButtonUp(GameSettings.NPC_SECONDARY_ACTION_KEY))
			{
				var clickObject = this.getClickObject();

				if (clickObject != null)
					clickObject.SendMessage("OnTarget", SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	#endregion

	#region Local Methods

	private GameObject getClickObject()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 100, this.layerMask))
			return hit.transform.gameObject;

		return null;
	}

	#endregion
};